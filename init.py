import pandas as pd
import math
import statistics
from scipy.optimize import fsolve
from data_loader import *

import numpy as np
# from physicalProperties import PhysicalProperties
# from processCompositions import ProcessCompositions
from flask import Flask, request
from flask_restful import Api, Resource, reqparse
# from data_processor import DataProcessor
# from functions import Solver
from controller import Controller
from X_property import XProperty
from thermodynamic_calculation import ThermodynamicCalculation



# def load_smr_wgs_ratedata(self):
#     df = pd.read_csv('smr_wgs_ratedata.csv')
#     df = df.pivot_table(columns='SMR_WGS_RATEDATA')
#     self.e = df.to_dict()['E']
#     self.a = df.to_dict()['A']
#     self.del_H_REF_298 = df.to_dict()['del_H_REF_298']
#     self.del_H_REF_948 = df.to_dict()['del_H_REF_948']
#     self.diff_noo = df.to_dict()['diff_noo']
#     self.CO_STOCH = df.to_dict()['CO_STOCH']
#     self.H2_STOCH = df.to_dict()['H2_STOCH']
#     self.reactant_STOCH = df.to_dict()['REACTANT_STOCH']
#     self.H2O_STOCH = df.to_dict()['H2O_STOCH']
#     self.CO2_STOCH = df.to_dict()['CO2_STOCH']
#     self.N2_STOCH = df.to_dict()['N2_STOCH']
#
#
# def load_smr_wgs_adsoptiondata(self):
#     df = pd.read_csv('smr_wgs_adsoptiondata.csv')
#     df = df.pivot_table(columns='SMR_WGS_ADSOPTIONDATA')
#     self.a_ad = df.to_dict()['A_ad']
#     self.del_H_ad = df.to_dict()['del_H_ad']