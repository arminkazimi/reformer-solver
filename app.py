from init import *

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('physicalProperties', type=dict, help='physicalProperties is required')
parser.add_argument('geometry', type=dict, help='physicalProperties is required')


class Landing(Resource):
    def post(self):
        args = parser.parse_args()
        firebox_list = args['geometry']['fireboxes']
        process_composition_list = args['physicalProperties']['processCompositions']
        process_stream_list = args['physicalProperties']['processStreams']
        gas_fuel_composition_list = args['physicalProperties']['gasFuelCompositions']
        # fire box state
        if firebox_list:

            main_stream_id = firebox_list[0]['mainStream'] if 'mainStream' in firebox_list[0] else None
            steam_id = firebox_list[0]['steam'] if 'steam' in firebox_list[0] else None

            main_stream = [item for item in process_stream_list if main_stream_id == item['id']][
                0] if main_stream_id else {}
            main_stream_process_composition = \
                [item for item in process_composition_list if main_stream['Composition ID#'] == item['id']][
                    0] if main_stream_id else {}
            steam = [item for item in process_stream_list if steam_id == item['id']][0] if steam_id else {}
            steam_process_composition = \
                [item for item in process_composition_list if steam['Composition ID#'] == item['id']][
                    0] if steam_id else {}
            if firebox_list[0]['calculationProcedure'] == 'fixedCombustion':
                fixed_combustion = dict()
                if 'fixedCombustion' in firebox_list[0]:
                    fixed_combustion['fixed_combustion'] = firebox_list[0]['fixedCombustion']

                fixed_combustion['gas_fuel_id_1'] = firebox_list[0]['gasFuel1ID'] if 'gasFuel1ID' in firebox_list[0] else None
                if 'gasFuel1Temp' in firebox_list[0]:
                    fixed_combustion['gas_fuel_temp_1'] = firebox_list[0]['gasFuel1Temp']
                if 'gasFuel1Flow' in firebox_list[0]:
                    fixed_combustion['gas_fuel_flow_1'] = firebox_list[0]['gasFuel1Flow']


                fixed_combustion['gas_fuel_id_2'] = firebox_list[0]['gasFuel2ID'] if 'gasFuel2ID' in firebox_list[0] else None
                if 'gasFuel2Temp' in firebox_list[0]:
                    fixed_combustion['gas_fuel_temp_2'] = firebox_list[0]['gasFuel2Temp']
                if 'gasFuel2Flow' in firebox_list[0]:
                    fixed_combustion['gas_fuel_flow_2'] = firebox_list[0]['gasFuel2Flow']


                fixed_combustion['gas_fuel_id_3'] = firebox_list[0]['gasFuel3ID'] if 'gasFuel3ID' in firebox_list[0] else None
                if 'gasFuel3Temp' in firebox_list[0]:
                    fixed_combustion['gas_fuel_temp_3'] = firebox_list[0]['gasFuel3Temp']
                if 'gasFuel3Flow' in firebox_list[0]:
                    fixed_combustion['gas_fuel_flow_3'] = firebox_list[0]['gasFuel3Flow']


                fixed_combustion['gas_fuel_id_4'] = firebox_list[0]['gasFuel4ID'] if 'gasFuel4ID' in firebox_list[0] else None
                if 'gasFuel4Temp' in firebox_list[0]:
                    fixed_combustion['gas_fuel_temp_4'] = firebox_list[0]['gasFuel4Temp']
                if 'gasFuel4Flow' in firebox_list[0]:
                    fixed_combustion['gas_fuel_flow_4'] = firebox_list[0]['gasFuel4Flow']
                res = ThermodynamicCalculation.fuel_property(calculation_procedure=fixed_combustion,
                                                             gas_fuel_component=gas_fuel_composition_list)
                print(res)

            main_stream_process_composition = XProperty.convert_mol_mass(
                main_stream_process_composition) if main_stream_id else {}
            steam_process_composition = XProperty.convert_mol_mass(steam_process_composition) if steam_id else {}

        stream_mixing_result = ThermodynamicCalculation.stream_mixing(main_stream, steam,
                                                                      main_stream_process_composition,
                                                                      steam_process_composition)

        z_process_result = XProperty.z_process(stream_mixing_result['t_process_in'], stream_mixing_result['p_process'],
                                               {key: value / 100 for key, value in
                                                stream_mixing_result['mol_y'].items()}, 1)

        res = ThermodynamicCalculation.heat_reaction({key: value / 100 for key, value in
                                                      stream_mixing_result['mol_y'].items()},
                                                     stream_mixing_result['mass_flow_in'],
                                                     stream_mixing_result['t_process_in'],
                                                     stream_mixing_result['p_process'],
                                                     1157,
                                                     z_process_result['mw_mixed'])

        if args['physicalProperties'] is None:
            print(args)
            return 'body is empty', 400

        return 1

    def get(self):
        return 'Hi, please use current URL and send POST request with body'


api.add_resource(Landing, '/')

if __name__ == '__main__':
    app.run(debug=True)
