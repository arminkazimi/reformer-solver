import pandas as pd
import math
from scipy.optimize import fsolve
import numpy as np


class DataProcessor:
    r_bar = 8.3144598e-2

    def __init__(self, data):
        self.data = data
        self.q = 0
        self.beta = 0
        self.eps = 0
        self.sigma = 0
        df = pd.read_csv('process_composition.csv', index_col=['process_composition'])
        self.t_c = df.to_dict()['Tc']
        self.t_b = df.to_dict()['Tb']
        self.p_c = df.to_dict()['Pc']
        self.v_c = {key: (value * 1e-3) for key, value in df.to_dict()['Vc'].items()}
        self.z_c = {key: (value * self.v_c[key] / (self.r_bar * self.t_c[key])) for key, value in self.p_c.items()}
        self.omega = df.to_dict()['omega']
        self.mw_comp = df.to_dict()['mw']
        self.a0_cp = df.to_dict()['a0cp']
        self.a1_cp = df.to_dict()['a1cp']
        self.a2cp = df.to_dict()['a2cp']
        self.a3_cp = df.to_dict()['a3cp']
        self.a4_cp = df.to_dict()['a4cp']
        self.pow0_cp = df.to_dict()['pow0_cp']
        self.pow1_cp = df.to_dict()['pow1_cp']
        self.pow2_cp = df.to_dict()['pow2_cp']
        self.pow3_cp = df.to_dict()['pow3_cp']
        self.pow4_cp = df.to_dict()['pow4_cp']

    def firebox_geometry_encode(self):
        box_type_dict = {'reformer': 2, 'heater': 3, 'gasifier': 4, 'adiabatic_reactors': 5, 'olefin': 6}
        box_geometry_dict = {'Box': 1, 'Cylindrical': 2, 'Side Wall': 3, 'terrace wall': 4}
        burner_direction_dict = {'upFiring': 3, 'downFiring': -3}
        tube_direction_dict = {'upToDown': 3, 'downToUp': -3}




        data = self.data['geometry']['fireboxes']
        print(data)
        return data

    def process_compositions_attachment_encode(self):
        df = pd.read_csv('process_composition.csv', index_col=['process_composition'])
        data_csv = df.to_dict()['mw']
        data = self.data['physicalProperties']['processCompositions']
        data_schema = self.data.copy()
        response = self.convert_mol_mass(data, data_csv, data_schema)
        return response

    def convert_mol_mass(self, data, data_csv, response):

        result_list = list()
        for obj in data:
            if obj['basis'] == 'mass':
                MASS_X_res = {key: value for key, value in obj.items() if
                              key != 'basis' and key != 'id' and key != 'name' and key != 'uuid'}
                MOL_Y_res = {key: float(100 * value / data_csv[key]) for key, value in MASS_X_res.items()}
                res = {key: float(value / data_csv[key]) for key, value in MASS_X_res.items()}
                MW = sum(res.values()) + 1e-8
                # print('MW object ', obj['id'], 'is: ', MW)
                MOL_Y_res = {key: float(value / MW) for key, value in MOL_Y_res.items()}
                # print('MOL Y res object ', obj['id'], 'is: ', MOL_Y_res)
                result = {'id': obj['id'], 'name': obj['name'] if obj['name'] else None,
                          'data': {'mass': MASS_X_res, 'mol': MOL_Y_res}}
                # result = {'id': obj['id'], 'data': {'mass': MASS_X_res, 'mol': MOL_Y_res}}
                result_list.append(result)
            elif obj['basis'] == 'mole':
                MOL_Y_res = {key: value for key, value in obj.items() if
                             key != 'basis' and key != 'id' and key != 'name' and key != 'uuid'}
                res = {key: value * data_csv[key] for key, value in MOL_Y_res.items()}
                MW = sum(res.values()) + 1e-8
                # print('MW object ', obj['id'], 'is: ', MW)
                MASS_X_res = {key: float(100 * data_csv[key] * value / MW) for key, value in MOL_Y_res.items()}
                # print('mass x res object ', obj['id'], 'is: ', MASS_X_res)
                result = {'id': obj['id'], 'name': obj['name'] if 'name' in obj.keys() else None,
                          'data': {'mass': MASS_X_res, 'mol': MOL_Y_res}}
                # result = {'id': obj['id'], 'data': {'mass': MASS_X_res, 'mol': MOL_Y_res}}
                result_list.append(result)
        response['physicalProperties']['processCompositions'].clear()
        response['physicalProperties']['processCompositions'] = result_list
        return response

    def stream_attachment_encode(self, process_stream):
        pass
        # return process_id, stream_data

    def z_process(self, EOS):
        process_compositions = self.data['physicalProperties']['processCompositions']
        process_streams = self.data['physicalProperties']['processStreams']

        t_process = process_streams[0]['Inlet Temprature (C)'] + 273.15
        p_process = process_streams[0]['Inlet Pressure (kPaa)']

        df = pd.read_csv('process_composition.csv', index_col=['process_composition'])
        result = 1
        data_schema = self.data.copy()

        r_bar = 8.3144598e-2
        r_kpa = 8.314
        s_compressibility_liquid = 0
        # pre_process...
        t_c = df.to_dict()['Tc']
        t_b = df.to_dict()['Tb']
        p_c = df.to_dict()['Pc']
        v_c = {key: (value * 1e-3) for key, value in df.to_dict()['Vc'].items()}
        z_c = {key: (value * v_c[key] / (r_bar * t_c[key])) for key, value in p_c.items()}
        omega = df.to_dict()['omega']
        mw_comp = df.to_dict()['mw']
        mass_mol_result = self.convert_mol_mass(process_compositions, mw_comp, data_schema)
        mol_y = mass_mol_result['physicalProperties']['processCompositions'][0]['data']['mol']
        mol_y = {key: value / 100 for key, value in mol_y.items()}
        mw_mixed = sum([value * mw_comp[key] for key, value in mol_y.items()])
        t_pc = sum([value * t_c[key] for key, value in mol_y.items()])
        v_pc = sum([value * v_c[key] for key, value in mol_y.items()])
        z_pc = sum([value * z_c[key] for key, value in mol_y.items()])
        omega_p = sum([value * omega[key] for key, value in mol_y.items()])
        p_pc = 100 * z_pc * r_bar * t_pc / v_pc
        p_pr = p_process / p_pc
        t_pr = t_process / t_pc

        if EOS == 1:
            OMEGA = 0.07779
            SAI = 0.45724
            self.eps = 1 - math.sqrt(2)
            self.sigma = 1 + math.sqrt(2)
            m = 0.37464 + 1.54226 * omega_p - 0.26992 * omega_p ** 2
            alpha = (1 + m * (1 - t_pr ** 0.5)) ** 2
            self.q = SAI * alpha / (OMEGA * t_pr)
            self.beta = OMEGA * p_pr / t_pr

            z_compressibility_gas = fsolve(self.z_func, np.ones(1))
        elif EOS == 2:
            OMEGA = 0.08664
            SAI = 0.45748
            self.eps = 0
            self.sigma = 1
            m = 0.48 + 1.574 * omega_p - 0.176 * omega_p ** 2
            alpha = (1 + m * (1 - t_pr ** 0.5)) ** 2
            self.q = SAI * alpha / (OMEGA * t_pr)
            self.beta = OMEGA * p_pr / t_pr

            z_compressibility_gas = fsolve(self.z_func, np.ones(1))
        elif EOS == 3:
            b0 = 0.083 - 0.422 / (t_pr ** 1.6)
            b1 = 0.139 - 0.172 / (t_pr ** 4.2)
            z0 = 1 + b0 * p_pr / t_pr
            z1 = b1 * p_pr / t_pr
            z_compressibility_gas = z0 + omega_p * z1
        print('z_compressibility_gas: ', z_compressibility_gas)
        return z_compressibility_gas

    def z_func(self, z):
        return (z - 1 - self.beta + self.q * self.beta * (z - self.beta) /
                ((z + self.sigma * self.beta) * (z + self.eps * self.beta)))

    def return_mol_y(self):
        process_compositions = self.data['physicalProperties']['processCompositions']
        data_schema = self.data.copy()
        mass_mol_result = self.convert_mol_mass(process_compositions, self.mw_comp, data_schema)
        mol_y = mass_mol_result['physicalProperties']['processCompositions'][0]['data']['mol']
        return mol_y
