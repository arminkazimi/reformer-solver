from init import *


class Controller:
    @staticmethod
    def pars_data(data):
        streaming = ['netflix', 'hulu', 'disney+', 'appletv+']
        platform = 'netflix'

        # if search(streaming, platform):
        #     print("Platform is found")
        # else:
        #     print("Platform does not found")
        if data['physicalProperties']:
            if data['physicalProperties']['processCompositions']:
                process_compositions_list = data['physicalProperties']['processCompositions']
                print('process compositions:    ', process_compositions_list)

            if data['physicalProperties']['processStreams']:
                process_stream = data['physicalProperties']['processStreams']
                print('process stream:    ', process_stream)

            if data['geometry']:
                geometry = data['geometry']
                if data['geometry']['fireboxes']:
                    fireboxes = data['geometry']['fireboxes']
                    print('fireboxes:    ', fireboxes)
            # fireboxes['stream']