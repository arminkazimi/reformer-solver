import pandas as pd

r_kpa = 8.314
r_bar = 8.3144598e-2
df = pd.read_csv('process_composition.csv', index_col=['process_composition'])
t_c = df.to_dict()['Tc']
t_b = df.to_dict()['Tb']
p_c = df.to_dict()['Pc']
v_c = {key: (value * 1e-3) for key, value in df.to_dict()['Vc'].items()}
z_c = {key: (value * v_c[key] / (r_bar * t_c[key])) for key, value in p_c.items()}
omega = df.to_dict()['omega']
mw_comp = df.to_dict()['mw']
a0_cp = df.to_dict()['a0cp']
a1_cp = df.to_dict()['a1cp']
a2_cp = df.to_dict()['a2cp']
a3_cp = df.to_dict()['a3cp']
a4_cp = df.to_dict()['a4cp']
carbon_molc = df.to_dict()['carbon_molc']
pow0_cp = df.to_dict()['pow0_cp']
pow1_cp = df.to_dict()['pow1_cp']
pow2_cp = df.to_dict()['pow2_cp']
pow3_cp = df.to_dict()['pow3_cp']
pow4_cp = df.to_dict()['pow4_cp']
dipole = df.to_dict()['dipole']
a_landa = df.to_dict()['A_landa']
b_landa = df.to_dict()['B_landa']
c_landa = df.to_dict()['C_landa']
d_landa = df.to_dict()['D_landa']

df = pd.read_csv('smr_wgs_ratedata.csv')
df = df.pivot_table(columns='SMR_WGS_RATEDATA')
e = df.to_dict()['E']
a = df.to_dict()['A']
del_H_REF_298 = df.to_dict()['del_H_REF_298']
del_H_REF_948 = df.to_dict()['del_H_REF_948']
diff_noo = df.to_dict()['diff_noo']
CO_STOCH = df.to_dict()['CO_STOCH']
H2_STOCH = df.to_dict()['H2_STOCH']
reactant_STOCH = df.to_dict()['REACTANT_STOCH']
H2O_STOCH = df.to_dict()['H2O_STOCH']
CO2_STOCH = df.to_dict()['CO2_STOCH']
N2_STOCH = df.to_dict()['N2_STOCH']

df = pd.read_csv('smr_wgs_adsoptiondata.csv')
df = df.pivot_table(columns='SMR_WGS_ADSORPTIONDATA')
a_ad = df.to_dict()['A_ad']
del_H_ad = df.to_dict()['del_H_ad']

df_kij = pd.read_csv('kij.csv', index_col=['kij'])
# H2 = df.to_dict()['H2']
# CH4 = df.to_dict()['CH4']
# H2O = df.to_dict()['H2O']
# CO2 = df.to_dict()['CO2']
# CO = df.to_dict()['CO']
# N2 = df.to_dict()['N2']
# Ar = df.to_dict()['Ar']
# NH3 = df.to_dict()['NH3']
# MeOH = df.to_dict()['MeOH']
# C2H6 = df.to_dict()['C2H6']
# C3H8 = df.to_dict()['C3H8']
# i_C4H10 = df.to_dict()['i-C4H10']
# n_C4H10 = df.to_dict()['n-C4H10']
# i_C5H12 = df.to_dict()['i-C5H12']
# n_C5H12 = df.to_dict()['n-C5H12']
# n_C6H14 = df.to_dict()['n-C6H14']
# N_HEPTANE = df.to_dict()['N-HEPTANE']
# one_OCTANE = df.to_dict()['1-OCTANE']
# METHYL_AMINE = df.to_dict()['METHYL-AMINE']
# CYCLOBUTANE = df.to_dict()['CYCLOBUTANE']
# ETHYL_AMINE = df.to_dict()['ETHYL-AMINE']
# HYDROGEN_CYANIDE = df.to_dict()['HYDROGEN-CYANIDE']
# CYCLOPENTANE = df.to_dict()['CYCLOPENTANE']
# TRIETHYLAMINE = df.to_dict()['TRIETHYLAMINE']
# two_BUTANOL = df.to_dict()['2-BUTANOL']
# TOLUENE = df.to_dict()['TOLUENE']
# N_BUTANOL = df.to_dict()['N-BUTANOL']
# two_PENTANOL = df.to_dict()['2-PENTANOL']
# BENZENE = df.to_dict()['BENZENE']
# CYCLOHEXANE = df.to_dict()['CYCLOHEXANE']
