import numpy as np

from init import *


class PhysicalProperties:
    def __init__(self):
        self.load_physical_properties()
        self.load_smr_wgs_ratedata()
        self.load_smr_wgs_adsoptiondata()

    def load_physical_properties(self):
        df = pd.read_csv('process_composition.csv', index_col=['process_composition'])
        self.t_c = df.to_dict()['Tc']
        self.t_b = df.to_dict()['Tb']
        self.p_c = df.to_dict()['Pc']
        self.v_c = {key: (value * 1e-3) for key, value in df.to_dict()['Vc'].items()}
        self.z_c = {key: (value * self.v_c[key] / (self.r_bar * self.t_c[key])) for key, value in self.p_c.items()}
        self.omega = df.to_dict()['omega']
        self.mw_comp = df.to_dict()['mw']
        self.a0_cp = df.to_dict()['a0cp']
        self.a1_cp = df.to_dict()['a1cp']
        self.a2_cp = df.to_dict()['a2cp']
        self.a3_cp = df.to_dict()['a3cp']
        self.a4_cp = df.to_dict()['a4cp']
        self.pow0_cp = df.to_dict()['pow0_cp']
        self.pow1_cp = df.to_dict()['pow1_cp']
        self.pow2_cp = df.to_dict()['pow2_cp']
        self.pow3_cp = df.to_dict()['pow3_cp']
        self.pow4_cp = df.to_dict()['pow4_cp']
        self.dipole = df.to_dict()['dipole']
        self.a_landa = df.to_dict()['A_landa']
        self.b_landa = df.to_dict()['B_landa']
        self.c_landa = df.to_dict()['C_landa']
        self.d_landa = df.to_dict()['D_landa']

    def load_smr_wgs_ratedata(self):
        df = pd.read_csv('smr_wgs_ratedata.csv')
        df = df.pivot_table(columns='SMR_WGS_RATEDATA')
        self.e = df.to_dict()['E']
        self.a = df.to_dict()['A']
        self.del_H_REF_298 = df.to_dict()['del_H_REF_298']
        self.del_H_REF_948 = df.to_dict()['del_H_REF_948']
        self.diff_noo = df.to_dict()['diff_noo']
        self.CO_STOCH = df.to_dict()['CO_STOCH']
        self.H2_STOCH = df.to_dict()['H2_STOCH']
        self.reactant_STOCH = df.to_dict()['REACTANT_STOCH']
        self.H2O_STOCH = df.to_dict()['H2O_STOCH']
        self.CO2_STOCH = df.to_dict()['CO2_STOCH']
        self.N2_STOCH = df.to_dict()['N2_STOCH']

    def load_smr_wgs_adsoptiondata(self):
        df = pd.read_csv('smr_wgs_adsoptiondata.csv')
        df = df.pivot_table(columns='SMR_WGS_ADSOPTIONDATA')
        self.a_ad = df.to_dict()['A_ad']
        self.del_H_ad = df.to_dict()['del_H_ad']

    @staticmethod
    def convert_mol_mass(self, data, data_csv, response):

        result_list = list()
        for obj in data:
            if obj['basis'] == 'mass':
                MASS_X_res = {key: value for key, value in obj.items() if
                              key != 'basis' and key != 'id' and key != 'name' and key != 'uuid'}
                MOL_Y_res = {key: float(100 * value / data_csv[key]) for key, value in MASS_X_res.items()}
                res = {key: float(value / data_csv[key]) for key, value in MASS_X_res.items()}
                MW = sum(res.values()) + 1e-8
                # print('MW object ', obj['id'], 'is: ', MW)
                MOL_Y_res = {key: float(value / MW) for key, value in MOL_Y_res.items()}
                # print('MOL Y res object ', obj['id'], 'is: ', MOL_Y_res)
                result = {'id': obj['id'], 'name': obj['name'] if obj['name'] else None,
                          'data': {'mass': MASS_X_res, 'mol': MOL_Y_res}}
                # result = {'id': obj['id'], 'data': {'mass': MASS_X_res, 'mol': MOL_Y_res}}
                result_list.append(result)
            elif obj['basis'] == 'mole':
                MOL_Y_res = {key: value for key, value in obj.items() if
                             key != 'basis' and key != 'id' and key != 'name' and key != 'uuid'}
                res = {key: value * data_csv[key] for key, value in MOL_Y_res.items()}
                MW = sum(res.values()) + 1e-8
                # print('MW object ', obj['id'], 'is: ', MW)
                MASS_X_res = {key: float(100 * data_csv[key] * value / MW) for key, value in MOL_Y_res.items()}
                # print('mass x res object ', obj['id'], 'is: ', MASS_X_res)
                result = {'id': obj['id'], 'name': obj['name'] if 'name' in obj.keys() else None,
                          'data': {'mass': MASS_X_res, 'mol': MOL_Y_res}}
                # result = {'id': obj['id'], 'data': {'mass': MASS_X_res, 'mol': MOL_Y_res}}
                result_list.append(result)
        response['physicalProperties']['processCompositions'].clear()
        response['physicalProperties']['processCompositions'] = result_list
        return response

    @staticmethod
    def residual_enthalpy(self, p_process, t_process, mol_y):
        t_pc = sum([value * self.t_c[key] for key, value in mol_y.items()])
        v_pc = sum([value * self.v_c[key] for key, value in mol_y.items()])
        z_pc = sum([value * self.z_c[key] for key, value in mol_y.items()])
        omega_p = sum([value * self.omega[key] for key, value in mol_y.items()])
        p_pc = 100 * z_pc * self.r_bar * t_pc / v_pc
        p_pr = p_process / p_pc
        t_pr = t_process / t_pc
        mw_prc = sum([value * self.mw_comp[key] for key, value in mol_y.items()])
        b0 = 0.083 - 0.422 / (t_pr ** 1.6)
        return "residual_enthalpy"

    @staticmethod
    def heat_capacity(self, temperature, mol_y):

        mw_comp = {key: self.mw_comp[key] for key, value in mol_y.items()}
        a0_cp = {key: self.a0_cp[key] for key, value in mol_y.items()}
        pow0_cp = {key: self.pow0_cp[key] for key, value in mol_y.items()}
        a1_cp = {key: self.a1_cp[key] for key, value in mol_y.items()}
        pow1_cp = {key: self.pow1_cp[key] for key, value in mol_y.items()}
        a2_cp = {key: self.a2_cp[key] for key, value in mol_y.items()}
        pow2_cp = {key: self.pow2_cp[key] for key, value in mol_y.items()}
        a3_cp = {key: self.a3_cp[key] for key, value in mol_y.items()}
        pow3_cp = {key: self.pow3_cp[key] for key, value in mol_y.items()}
        a4_cp = {key: self.a4_cp[key] for key, value in mol_y.items()}
        pow4_cp = {key: self.pow4_cp[key] for key, value in mol_y.items()}

        mw_comp = np.array(list(mw_comp.values())).reshape(len(mol_y), 1)

        a0_cp = np.array(list(a0_cp.values())).reshape(len(mol_y), 1)
        pow0_cp = np.array(list(pow0_cp.values())).reshape(len(mol_y), 1)
        a1_cp = np.array(list(a1_cp.values())).reshape(len(mol_y), 1)
        pow1_cp = np.array(list(pow1_cp.values())).reshape(len(mol_y), 1)
        a2_cp = np.array(list(a2_cp.values())).reshape(len(mol_y), 1)
        pow2_cp = np.array(list(pow2_cp.values())).reshape(len(mol_y), 1)
        a3_cp = np.array(list(a3_cp.values())).reshape(len(mol_y), 1)
        pow3_cp = np.array(list(pow3_cp.values())).reshape(len(mol_y), 1)
        a4_cp = np.array(list(a4_cp.values())).reshape(len(mol_y), 1)
        pow4_cp = np.array(list(pow4_cp.values())).reshape(len(mol_y), 1)
        mol_y_arr = np.array(list(mol_y.values())).reshape(len(mol_y), 1)
        temperature = np.array(temperature * len(mol_y)).reshape(len(mol_y), len(temperature))

        cp_comp = mw_comp * (
                a0_cp * temperature ** pow0_cp + a1_cp * temperature ** pow1_cp + a2_cp * temperature ** pow2_cp + a3_cp * temperature ** pow3_cp + a4_cp * temperature ** pow4_cp)

        cp_g_mol = sum(mol_y_arr * cp_comp)

        cp_g_mass = cp_g_mol / sum(mol_y_arr * mw_comp)
        print(cp_g_mass.shape)
        return "heat_capacity"

    # def stream_mixing(self, stream_data):
    #     idx = []
    #     idxx = []
    #     if stream_data:
    #         mass_x0 = np.ones(30, 1)
    #         mass_steam_flow0 = 1
    #         mass_flow_in0 = 1
    #         p_process0 = 1
    #         t_process_in0 = 1
    #         mass_x1 = np.ones(30, 1)
    #         mass_steam_flow1 = 1
    #         mass_flow_in1 = 1
    #         p_process1 = 1
    #         t_process_in1 = 1
    #         pass
    #     else:
    #         mass_x0 = np.zeros(30, 1)
    #         mass_steam_flow0 = 0
    #         mass_flow_in0 = 0
    #         p_process0 = 0
    #         t_process_in0 = 0
    #         mass_x1 = np.zeros(30, 1)
    #         mass_steam_flow1 = 0
    #         mass_flow_in1 = 0
    #         p_process1 = 0
    #         t_process_in1 = 0
    #
    #     mass_flow_in0 += 1e-6
    #     mass_flow_in = mass_flow_in1 + mass_flow_in0 + mass_steam_flow0 + mass_steam_flow1
    #     mass_x = (mass_x1 * mass_flow_in1 + mass_x0 * mass_flow_in0) / 100
    #     mass_x['H2O'] = mass_x['H2O'] + mass_steam_flow1 + mass_steam_flow0
    #     mass_x = 100 * mass_x / mass_flow_in
    #     mol_y = 100 * (mass_x / self.mw_comp) / sum(mass_x / self.mw_comp + 1e-8)
    #
    #     mass_flow_comp0 = mass_x0 * mass_flow_in0 / 100
    #     mass_flow_comp0['H2O'] += mass_steam_flow0
    #     mass_x0 = 100 * mass_flow_comp0 / (mass_flow_in0 + mass_steam_flow0)
    #     mol_y0 = 100 * (mass_x0 / self.mw_comp) / sum(mass_x0 / self.mw_comp + 1e-8)
    #
    #     mass_flow_comp1 = mass_x1 * mass_flow_in1 / 100
    #     mass_flow_comp1['H2O'] += mass_steam_flow1
    #     mass_x1 = 100 * mass_flow_comp1 / (mass_flow_in1 + mass_steam_flow1)
    #     mol_y1 = 100 * (mass_x1 / self.mw_comp) / sum(mass_x1 / self.mw_comp + 1e-8)
    #
    #     # if idx & idxx:
