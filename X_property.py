import numpy as np

from init import *


class XProperty:

    @staticmethod
    def convert_mol_mass(obj):
        if obj['basis'] == 'mass':

            mass_x_res = {key: value for key, value in obj.items() if
                          key != 'basis' and key != 'id' and key != 'name' and key != 'uuid'}
            mol_y_res = {key: float(100 * value / mw_comp[key]) for key, value in mass_x_res.items()}
            res = {key: float(value / mw_comp[key]) for key, value in mass_x_res.items()}
            mw = sum(res.values()) + 1e-8
            mol_y_res = {key: float(value / mw) for key, value in mol_y_res.items()}
            result = {'id': obj['id'] if 'id' in obj.keys() else None,
                      'name': obj['name'] if 'name' in obj.keys() else None,
                      'mass': mass_x_res, 'mol': mol_y_res}
            return result

        elif obj['basis'] == 'mole':

            mol_y_res = {key: value for key, value in obj.items() if
                         key != 'basis' and key != 'id' and key != 'name' and key != 'uuid'}
            res = {key: value * mw_comp[key] for key, value in mol_y_res.items()}
            mw = sum(res.values()) + 1e-8
            mass_x_res = {key: float(100 * mw_comp[key] * value / mw) for key, value in mol_y_res.items()}
            result = {'id': obj['id'] if 'id' in obj.keys() else None,
                      'name': obj['name'] if 'name' in obj.keys() else None,
                      'mass': mass_x_res, 'mol': mol_y_res}
            return result

    @staticmethod
    def residual_enthalpy(p_process, t_process, mol_y):
        t_pc = sum([value * t_c[key] for key, value in mol_y.items()])
        v_pc = sum([value * v_c[key] for key, value in mol_y.items()])
        z_pc = sum([value * z_c[key] for key, value in mol_y.items()])
        omega_p = sum([value * omega[key] for key, value in mol_y.items()])
        p_pc = 100 * z_pc * r_bar * t_pc / v_pc
        p_pr = np.array([value / p_pc for value in p_process])
        t_pr = np.array([value / t_pc for value in t_process])
        mw_prc = sum([value * mw_comp[key] for key, value in mol_y.items()])
        b0 = np.array([0.083 - 0.422 / (value ** 1.6) for value in t_pr])
        db0_dtr = np.array([0.675 / value ** 2.6 for value in t_pr])
        b1 = np.array([0.139 - 0.172 / value ** 4.2 for value in t_pr])
        db1_dtr = np.array([0.722 / value ** 5.2 for value in t_pr])
        h_residual = r_bar * t_pc * p_pr * (b0 - t_pr * db0_dtr + omega_p * (b1 - t_pr * db1_dtr)) / mw_prc
        s_residual = r_bar * p_pr * (db0_dtr + omega_p * db1_dtr) / mw_prc
        return h_residual, s_residual

    @staticmethod
    def heat_capacity(temperature, mol_y, temperature_ref=298.15):
        mw_comp_selected = {key: mw_comp[key] for key, value in mol_y.items()}
        a0_cp_selected = {key: a0_cp[key] for key, value in mol_y.items()}
        pow0_cp_selected = {key: pow0_cp[key] for key, value in mol_y.items()}
        a1_cp_selected = {key: a1_cp[key] for key, value in mol_y.items()}
        pow1_cp_selected = {key: pow1_cp[key] for key, value in mol_y.items()}
        a2_cp_selected = {key: a2_cp[key] for key, value in mol_y.items()}
        pow2_cp_selected = {key: pow2_cp[key] for key, value in mol_y.items()}
        a3_cp_selected = {key: a3_cp[key] for key, value in mol_y.items()}
        pow3_cp_selected = {key: pow3_cp[key] for key, value in mol_y.items()}
        a4_cp_selected = {key: a4_cp[key] for key, value in mol_y.items()}
        pow4_cp_selected = {key: pow4_cp[key] for key, value in mol_y.items()}

        mw_comp_selected = np.array(list(mw_comp_selected.values())).reshape(len(mol_y), 1)

        a0_cp_selected = np.array(list(a0_cp_selected.values())).reshape(len(mol_y), 1)
        pow0_cp_selected = np.array(list(pow0_cp_selected.values())).reshape(len(mol_y), 1)
        a1_cp_selected = np.array(list(a1_cp_selected.values())).reshape(len(mol_y), 1)
        pow1_cp_selected = np.array(list(pow1_cp_selected.values())).reshape(len(mol_y), 1)
        a2_cp_selected = np.array(list(a2_cp_selected.values())).reshape(len(mol_y), 1)
        pow2_cp_selected = np.array(list(pow2_cp_selected.values())).reshape(len(mol_y), 1)
        a3_cp_selected = np.array(list(a3_cp_selected.values())).reshape(len(mol_y), 1)
        pow3_cp_selected = np.array(list(pow3_cp_selected.values())).reshape(len(mol_y), 1)
        a4_cp_selected = np.array(list(a4_cp_selected.values())).reshape(len(mol_y), 1)
        pow4_cp_selected = np.array(list(pow4_cp_selected.values())).reshape(len(mol_y), 1)
        mol_y_arr = np.array(list(mol_y.values())).reshape(len(mol_y), 1)
        temperature = np.array(temperature * len(mol_y)).reshape(len(mol_y), len(temperature))
        temperature -= 273.15
        cp_comp = mw_comp_selected * (
                a0_cp_selected * temperature ** pow0_cp_selected + a1_cp_selected * temperature ** pow1_cp_selected + a2_cp_selected * temperature ** pow2_cp_selected + a3_cp_selected * temperature ** pow3_cp_selected + a4_cp_selected * temperature ** pow4_cp_selected)
        cp_g_mol = sum(mol_y_arr * cp_comp)
        cp_g_mass = cp_g_mol / sum(mol_y_arr * mw_comp_selected)

        # % % % 1/(T-temperature_ref)*int(Cp*dT)
        int_cp_comp_temperature_ref = mw_comp_selected * (
                a0_cp_selected / (pow0_cp_selected + 1) * temperature_ref ** (pow0_cp_selected + 1) + a1_cp_selected / (
                pow1_cp_selected + 1) * temperature_ref ** (pow1_cp_selected + 1) + a2_cp_selected / (
                        pow2_cp_selected + 1) * temperature_ref ** (
                        pow2_cp_selected + 1) + a3_cp_selected / (pow3_cp_selected + 1) * temperature_ref ** (
                        pow3_cp_selected + 1) + a4_cp_selected / (pow4_cp_selected + 1) * temperature_ref ** (
                        pow4_cp_selected + 1))  # %%kj/kmol.K

        int_cp_comp_temperature = mw_comp_selected * (
                a0_cp_selected / (pow0_cp_selected + 1) * temperature ** (pow0_cp_selected + 1) + a1_cp_selected / (
                pow1_cp_selected + 1) * temperature ** (
                        pow1_cp_selected + 1) + a2_cp_selected / (pow2_cp_selected + 1) * temperature ** (
                        pow2_cp_selected + 1) + a3_cp_selected / (
                        pow3_cp_selected + 1) * temperature ** (pow3_cp_selected + 1) + a4_cp_selected / (
                        pow4_cp_selected + 1) * temperature ** (
                        pow4_cp_selected + 1))  # %%kj/kmol.K
        # print('mol_y_arr    ', mol_y_arr)
        # print('int_cp_comp_temperature    ', int_cp_comp_temperature)
        # print('int_cp_comp_temperature_ref    ', int_cp_comp_temperature_ref)
        # print('temperature    ', temperature.shape)
        # print('temperature_ref    ', temperature_ref)
        ave_cp_g_mol_heat = sum(
            mol_y_arr * (int_cp_comp_temperature - int_cp_comp_temperature_ref) / (temperature - temperature_ref))
        ave_cp_g_mass_heat = ave_cp_g_mol_heat / sum(mol_y_arr * mw_comp_selected)  # %%kj/kg/K;
        # % % % 1/T*int(Cp*dT)
        int_cp_comps_temperature_ref = mw_comp_selected * (
                a0_cp_selected / (pow0_cp_selected + 1) * temperature_ref ** (
                pow0_cp_selected + 1) + a1_cp_selected / (
                        pow1_cp_selected + 1) * temperature_ref ** (pow1_cp_selected + 1) + a2_cp_selected / (
                        pow2_cp_selected + 1) * temperature_ref ** (
                        pow2_cp_selected + 1) + a3_cp_selected / (
                        pow3_cp_selected + 1) * temperature_ref ** (
                        pow3_cp_selected + 1) + a4_cp_selected / (
                        pow4_cp_selected + 1) * temperature_ref ** (
                        pow4_cp_selected + 1))  # %%kj/kmol.K

        int_cp_comps_temperature = mw_comp_selected * (
                a0_cp_selected / (pow0_cp_selected + 1) * temperature ** (pow0_cp_selected + 1) + a1_cp_selected / (
                pow1_cp_selected + 1) * temperature ** (
                        pow1_cp_selected + 1) + a2_cp_selected / (pow2_cp_selected + 1) * temperature ** (
                        pow2_cp_selected + 1) + a3_cp_selected / (
                        pow3_cp_selected + 1) * temperature ** (pow3_cp_selected + 1) + a4_cp_selected / (
                        pow4_cp_selected + 1) * temperature ** (
                        pow4_cp_selected + 1))  # %%kj/kmol.K
        cp_g_mol_heat_temp = sum(
            mol_y_arr * (int_cp_comps_temperature - int_cp_comps_temperature_ref) / (temperature + 273.15),
            1)  # %% [kj/kmol/K] ()
        cp_g_mass_heat_temp = (cp_g_mol_heat_temp / sum(mol_y_arr * mw_comp_selected))  # %% [kj/kmol/K]

        # % % % int(Cp*dT/T)
        int_cp_comps_temperature_ref = mw_comp_selected * (
                a0_cp_selected / (pow0_cp_selected + 1) * temperature_ref ** (
                pow0_cp_selected + 0) + a1_cp_selected / (
                        pow1_cp_selected + 1) * temperature_ref ** (pow1_cp_selected + 0) + a2_cp_selected / (
                        pow2_cp_selected + 1) * temperature_ref ** (
                        pow2_cp_selected + 0) + a3_cp_selected / (
                        pow3_cp_selected + 1) * temperature_ref ** (
                        pow3_cp_selected + 0) + a4_cp_selected / (
                        pow4_cp_selected + 1) * temperature_ref ** (
                        pow4_cp_selected + 0))  # %%kj/kmol.K
        int_cp_comps_temperature = mw_comp_selected * (
                a0_cp_selected / (pow0_cp_selected + 1) * temperature ** (pow0_cp_selected + 0) + a1_cp_selected / (
                pow1_cp_selected + 1) * temperature ** (
                        pow1_cp_selected + 0) + a2_cp_selected / (pow2_cp_selected + 1) * temperature ** (
                        pow2_cp_selected + 0) + a3_cp_selected / (
                        pow3_cp_selected + 1) * temperature ** (pow3_cp_selected + 0) + a4_cp_selected / (
                        pow4_cp_selected + 0) * temperature ** (
                        pow4_cp_selected + 0))  # %%kj/kmol.K
        ave_cp_g_mol_entropic = sum(mol_y_arr * (int_cp_comps_temperature - int_cp_comps_temperature_ref))
        ave_cp_g_mass_entropic = ave_cp_g_mol_entropic / sum(mol_y_arr * mw_comp_selected)

        return cp_g_mol, cp_g_mass, ave_cp_g_mol_heat, ave_cp_g_mass_heat, cp_g_mol_heat_temp, cp_g_mass_heat_temp, ave_cp_g_mol_entropic, ave_cp_g_mass_entropic
        # print(cp_g_mass.shape)

    @staticmethod
    def converting_heavy_hydrocarbons(molar_flow_comp, eps0, eps1):
        molar_flow_comp['H2'] = molar_flow_comp['H2'] if 'H2' in molar_flow_comp else 0
        molar_flow_comp['CH4'] = molar_flow_comp['CH4'] if 'CH4' in molar_flow_comp else 0
        molar_flow_comp['H2O'] = molar_flow_comp['H2O'] if 'H2O' in molar_flow_comp else 0
        molar_flow_comp['CO2'] = molar_flow_comp['CO2'] if 'CO2' in molar_flow_comp else 0
        molar_flow_comp['CO'] = molar_flow_comp['CO'] if 'CO' in molar_flow_comp else 0
        molar_flow_comp['N2'] = molar_flow_comp['N2'] if 'N2' in molar_flow_comp else 0

        del_H_NON_CH4 = sum([value * del_H_REF_298[key] for key, value in molar_flow_comp.items() if
                             key not in {'H2', 'CH4', 'H2O', 'CO2', 'CO', 'N2'}])
        eps_heavy = {key: -value / reactant_STOCH[key] for key, value in molar_flow_comp.items() if
                     key not in {'H2', 'CH4', 'H2O', 'CO2', 'CO', 'N2'}}
        molar_flow_in = sum(molar_flow_comp.values())
        # mol_y_in = dict()
        denominator = molar_flow_in + 2 * eps0 + 2 * eps1 + sum(
            [value * diff_noo[key] for key, value in eps_heavy.items()])
        mol_y_in = {key: (molar_flow_comp[key] + (value * reactant_STOCH[key])) / denominator for key, value in
                    eps_heavy.items()}

        mol_y_in['H2'] = (molar_flow_comp['H2'] + 3 * eps0 + 4 * eps1 + sum(
            [value * H2_STOCH[key] for key, value in eps_heavy.items()])) / denominator
        mol_y_in['CH4'] = (molar_flow_comp['CH4'] - 1 * eps0 - 1 * eps1) / denominator
        mol_y_in['H2O'] = (molar_flow_comp['H2O'] - 1 * eps0 - 2 * eps1 - sum(
            [value * H2O_STOCH[key] for key, value in eps_heavy.items()])) / denominator
        mol_y_in['CO2'] = (molar_flow_comp['CO2'] + 0 * eps0 + 1 * eps1 + sum(
            [value * CO2_STOCH[key] for key, value in eps_heavy.items()])) / denominator
        mol_y_in['CO'] = (molar_flow_comp['CO'] + 1 * eps0 - 0 * eps1 + sum(
            [value * CO_STOCH[key] for key, value in eps_heavy.items()])) / denominator
        mol_y_in['N2'] = (molar_flow_comp['N2'] + sum(
            [value * N2_STOCH[key] for key, value in eps_heavy.items()])) / denominator

        return mol_y_in, del_H_NON_CH4

    @staticmethod
    def z_process(t_process, p_process, mol_y, EOS):

        z_compressibility_liquid = 0
        # pre_process...

        mw_mixed = sum([value * mw_comp[key] for key, value in mol_y.items()])
        t_pc = sum([value * t_c[key] for key, value in mol_y.items()])
        v_pc = sum([value * v_c[key] for key, value in mol_y.items()])
        z_pc = sum([value * z_c[key] for key, value in mol_y.items()])
        omega_p = sum([value * omega[key] for key, value in mol_y.items()])
        p_pc = 100 * z_pc * r_bar * t_pc / v_pc
        p_pr = p_process / p_pc
        t_pr = t_process / t_pc

        if EOS == 1:
            OMEGA = 0.07779
            SAI = 0.45724
            eps = 1 - math.sqrt(2)
            sigma = 1 + math.sqrt(2)
            m = 0.37464 + 1.54226 * omega_p - 0.26992 * omega_p ** 2
            alpha = (1 + m * (1 - t_pr ** 0.5)) ** 2
            q = SAI * alpha / (OMEGA * t_pr)
            beta = OMEGA * p_pr / t_pr

            def z_func(z):
                return (z - 1 - beta + q * beta * (z - beta) /
                        ((z + sigma * beta) * (z + eps * beta)))

            z_compressibility_gas = fsolve(z_func, np.ones(1))
        elif EOS == 2:
            OMEGA = 0.08664
            SAI = 0.45748
            eps = 0
            sigma = 1
            m = 0.48 + 1.574 * omega_p - 0.176 * omega_p ** 2
            alpha = (1 + m * (1 - t_pr ** 0.5)) ** 2
            q = SAI * alpha / (OMEGA * t_pr)
            beta = OMEGA * p_pr / t_pr

            def z_func(z):
                return (z - 1 - beta + q * beta * (z - beta) /
                        ((z + sigma * beta) * (z + eps * beta)))

            z_compressibility_gas = fsolve(z_func, np.ones(1))
        elif EOS == 3:
            b0 = 0.083 - 0.422 / (t_pr ** 1.6)
            b1 = 0.139 - 0.172 / (t_pr ** 4.2)
            z0 = 1 + b0 * p_pr / t_pr
            z1 = b1 * p_pr / t_pr
            z_compressibility_gas = z0 + omega_p * z1
        density_gas = p_process * mw_mixed / (z_compressibility_gas * r_kpa * t_process)
        if z_compressibility_liquid != 0:
            density_liquid = p_process * mw_mixed / (z_compressibility_liquid * r_kpa * t_process)
        else:
            density_liquid = 0

        return {'z_compressibility_gas': z_compressibility_gas, 'z_compressibility_liquid': z_compressibility_liquid,
                'density_gas': density_gas, 'density_liquid': density_liquid, 'mw_mixed': mw_mixed}

    # def z_func(z):
    #     return (z - 1 - beta + q * beta * (z - beta) /
    #             ((z + sigma * beta) * (z + eps * beta)))
    @staticmethod
    def gas_viscosity_conductivity(temperature, mol_y):
        f_q = mol_y
        mw_comp_selected = {key: mw_comp[key] for key, value in mol_y.items()}
        t_c_selected = {key: t_c[key] for key, value in mol_y.items()}
        p_c_selected = {key: p_c[key] for key, value in mol_y.items()}
        dipole_selected = {key: dipole[key] for key, value in mol_y.items()}

        a_landa_selected = {key: a_landa[key] for key, value in mol_y.items()}
        b_landa_selected = {key: b_landa[key] for key, value in mol_y.items()}
        c_landa_selected = {key: c_landa[key] for key, value in mol_y.items()}
        d_landa_selected = {key: d_landa[key] for key, value in mol_y.items()}
        for t in range(temperature):
            tr = t / t_c
            vis_r = 52. * dipole_selected * p_c_selected / (t_c_selected ** 2)
            kesi = 0.176 * (t_c_selected / (mw_comp_selected ** 3) * (p_c_selected ** 4)) ** 1.6
            f_p = np.sign(np.sign(0.022 - vis_r) + 1) * 1 + np.sign(
                np.sign(0.075 - vis_r) * np.sign(vis_r - 0.0220001) + 1) * (1 + 30.55 * (292 - z_c) ** 1.72) * abs(
                0.96 + 1 * (tr - 0.7))
            if 'H2' in f_q.keys():
                pass
            viscosity_comp = f_p * f_q / kesi * (0.807 * tr ** 0.618 - 0.357 * math.exp(-0.449 * tr) + 0.34 * math.exp(
                -4.058 * tr) + 0.018) * 1e-7
            phi_ij = ((1 + ((viscosity_comp / (viscosity_comp)) ** 0.5) * (
                    mw_comp_selected / mw_comp_selected) ** 0.25) ** 2) / (
                             8 * (1 + (mw_comp_selected / mw_comp_selected)) ** 0.5)
            landa_comp = a_landa + b_landa * t + c_landa * t ** 2 + d_landa * t ** 3
            thermal_conductivity_g = 0
            viscosity_g = 0
            for mol in mol_y:
                thermal_conductivity_g = thermal_conductivity_g + (mol * landa_comp / sum(mol_y * phi_ij))
                viscosity_g = viscosity_g + (mol * viscosity_comp / sum(mol_y * phi_ij))



    @staticmethod
    def pressure_drop(pressuredropequation2, pipe_lenght_eff_pressuredrop, pipe_inner_dia, m_dot_tube,
                      process_inlet_pressure, viscosity_g_bulk, density_g_bulk,
                      diam_cylindricalcatalyt, length_cylindricalcatalyt, number_internal_holes, holediameter1,
                      diam_sphericalcatalyt, diam_other_calcatalyt, length_other_catalyt, catalyst_shape, porosity_tube,
                      pressure_drop_factor, tube_direction):

        if not porosity_tube:
            porosity_tube = 0.45
        elif porosity_tube > 1:
            porosity_tube = porosity_tube / 100

        if not catalyst_shape:
            catalyst_shape = 3
        if not diam_other_calcatalyt:
            diam_other_calcatalyt = 0.01
        if not length_other_catalyt:
            length_other_catalyt = 0.01

        for i in range(4):
            if catalyst_shape == 1:
                catal_volume = 1 / 6 * pi * diam_sphericalcatalyt[i] ** 3 - 1 / 4 * pi * number_internal_holes[i] * \
                               holediameter1[i] ** 2 * diam_sphericalcatalyt[i]
                catal_ext_surf = pi * diam_sphericalcatalyt[i] ** 2 - 1 / 4 * number_internal_holes[i] * pi * \
                                 holediameter1[i] ** 2 + number_internal_holes[i] * pi * holediameter1[i] * \
                                 diam_sphericalcatalyt[i]
                catal_equival_diam[i] = (catal_ext_surf / pi) ^ (1 / 2)
                catal_equival_length[i] = (6 * catal_volume / pi) ^ (1 / 3)

            elif catalyst_shape == 2:
                catal_volume = 1 / 4 * pi * diam_cylindricalcatalyt[i] ** 2 * length_cylindricalcatalyt[
                    i] - 1 / 4 * pi * \
                               number_internal_holes[i] * holediameter1[i] ^ 2 * length_cylindricalcatalyt[i]
                catal_ext_surf = pi * diam_cylindricalcatalyt[i] * length_cylindricalcatalyt[i] + pi * \
                                 number_internal_holes[
                                     i] * holediameter1[i] * length_cylindricalcatalyt[i] - 1 / 4 * pi * \
                                 number_internal_holes[
                                     i] * holediameter1(i) ** 2

                catal_equival_diam[i] = (catal_ext_surf / pi) ** (1 / 2)
                catal_equival_length[i] = (6 * catal_volume / pi) ** (1 / 3)

            elif catalyst_shape == 3:

                catal_equival_diam[i] = diam_othercalcatalyt[i]
                catal_equival_length[i] = length_othercatalyt[i]
