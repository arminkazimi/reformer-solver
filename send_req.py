import requests
import json

base = 'http://127.0.0.1:5000/'
# file_path = os.path.join(data_path, 'test.json')
with open('test.json') as f:
    data = json.load(f)

# print(json.dumps(data))
body = json.dumps(data)
response = requests.post(base + 'reformer', body)
print(response.json())
