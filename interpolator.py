import pandas as pd
import numpy as np
from scipy.interpolate import griddata
from scipy.interpolate import LinearNDInterpolator

df = pd.read_csv('gg_table.csv').values
points = df[:500, :4]
values = df[:500, 4:5].reshape(500)
request = np.array([[430, 7, 0.6, 0.75]])
linInter = LinearNDInterpolator(points, values)
print(linInter(request))
# print(griddata(points, values, request))
