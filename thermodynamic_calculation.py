import numpy as np

from init import *


class ThermodynamicCalculation:
    @staticmethod
    def stream_mixing(stream_data={}, steam_data={}, mass_x_res_stream={}, mass_x_res_steam={}):
        # t_process_in, p_process = 0, 0
        stream_flag = False
        steam_flag = False
        if stream_data:
            stream_flag = True
            t_process_in0 = float(stream_data['Inlet Temprature (C)']) + 273.15
            p_process0 = float(
                stream_data['Inlet Pressure (kPaa)']) if 'Inlet Pressure (kPaa)' in stream_data.keys() else 0
            mass_flow_in0 = float(
                stream_data['Mass flow rate (kg/hr)']) if 'Mass flow rate (kg/hr)' in stream_data.keys() else 0
            mass_steam_flow0 = float(
                stream_data['Steam flow rate (kg/hr)']) if 'Steam flow rate (kg/hr)' in stream_data.keys() else 0
            mass_x0 = mass_x_res_stream['mass']

        else:
            mass_x0 = np.zeros(len(mass_x_res_stream['mass']), 1)
            mass_steam_flow0 = 1e-8
            mass_flow_in0 = 1e-8
            p_process0 = 0
            t_process_in0 = 0

        if steam_data:
            steam_flag = True
            t_process_in1 = float(steam_data['Inlet Temprature (C)']) + 273.15
            p_process1 = float(
                steam_data['Inlet Pressure (kPaa)']) if 'Inlet Pressure (kPaa)' in steam_data.keys() else 0
            mass_flow_in1 = float(
                steam_data['Mass flow rate (kg/hr)']) if 'Mass flow rate (kg/hr)' in steam_data.keys() else 0
            mass_steam_flow1 = float(
                steam_data['Steam flow rate (kg/hr)']) if 'Steam flow rate (kg/hr)' in steam_data.keys() else 0
            mass_x1 = mass_x_res_steam['mass']
        else:

            mass_x1 = {}
            mass_steam_flow1 = 1e-8
            mass_flow_in1 = 1e-8
            p_process1 = 0
            t_process_in1 = 0

        mass_flow_in = mass_flow_in1 + mass_flow_in0 + mass_steam_flow0 + mass_steam_flow1
        keys = set(list(mass_x1.keys()) + list(mass_x0.keys()))
        mass_x0 = {key: mass_x0[key] if key in mass_x0.keys() else 0 for key in keys}
        mass_x1 = {key: mass_x1[key] if key in mass_x1.keys() else 0 for key in keys}
        mass_x = {key: (mass_x1[key] * mass_flow_in1 + mass_x0[key] * mass_flow_in0) / 100 for key in keys}

        mass_x['H2O'] = mass_x['H2O'] + mass_steam_flow1 + mass_steam_flow0

        # mass_x = 100 * mass_x / mass_flow_in
        mass_x = {key: 100 * value / mass_flow_in for key, value in mass_x.items()}
        mass_x_copy = mass_x.copy()
        mass_x_copy['basis'] = 'mass'
        mol_y = XProperty.convert_mol_mass(mass_x_copy)['mol']

        mass_flow_comp0 = {key: value * mass_flow_in0 / 100 for key, value in mass_x0.items()}
        mass_flow_comp0['H2O'] += mass_steam_flow0

        mass_x0 = {key: 100 * value / (mass_flow_in0 + mass_steam_flow0) for key, value in mass_flow_comp0.items()}

        mass_x0_copy = mass_x0.copy()
        mass_x0_copy['basis'] = 'mass'
        mol_y0 = XProperty.convert_mol_mass(mass_x0_copy)['mol']

        mass_flow_comp1 = {key: value * mass_flow_in1 / 100 for key, value in mass_x1.items()}
        mass_flow_comp1['H2O'] += mass_steam_flow1
        # mass_x1 = 100 * mass_flow_comp1 / (mass_flow_in1 + mass_steam_flow1)
        mass_x1 = {key: 100 * value / (mass_flow_in1 + mass_steam_flow1) for key, value in mass_flow_comp1.items()}
        mass_x1_copy = mass_x1.copy()
        mass_x1_copy['basis'] = 'mass'
        mol_y1 = XProperty.convert_mol_mass(mass_x1_copy)['mol']

        if steam_flag and stream_flag:

            if p_process1 == 0 and p_process0 != 0:
                p_process = p_process0
            elif p_process1 != 0 and p_process0 == 0:
                p_process = p_process1
            elif p_process1 != 0 and p_process0 != 0:
                p_process = min(p_process0, p_process1)
            else:
                p_process = 0
            t0 = np.linspace(298.15, t_process_in0, 100).tolist()
            result_heat_capacity0 = XProperty.heat_capacity(t0,
                                                            {key: value / 100 for key, value in mol_y0.items()})
            # result_heat_capacity1 = XProperty.heat_capacity([t_process_in0],
            #                                                 {key: value / 100 for key, value in mol_y1.items()})
            result_residual_enthalpy0 = XProperty.residual_enthalpy([p_process0], [t_process_in0],
                                                                    {key: value / 100 for key, value in mol_y0.items()})
            t1 = np.linspace(298.15, t_process_in1, 100).tolist()
            result_heat_capacity1 = XProperty.heat_capacity(t1,
                                                            {key: value / 100 for key, value in mol_y1.items()})
            # result_heat_capacity1 = XProperty.heat_capacity([t_process_in1],
            #                                                 {key: value / 100 for key, value in mol_y1.items()})
            result_residual_enthalpy1 = XProperty.residual_enthalpy([p_process1], [t_process_in1],
                                                                    {key: value / 100 for key, value in mol_y1.items()})

            cp_g_feed0 = statistics.mean(result_heat_capacity0[1]) if result_heat_capacity0[1].tolist() else 0
            cp_g_feed1 = statistics.mean(result_heat_capacity1[1]) if result_heat_capacity1[1].tolist() else 0
            h_residual0 = result_residual_enthalpy0[0]
            h_residual1 = result_residual_enthalpy1[0]

            del_h_reactant0 = (cp_g_feed0 * (t_process_in0 - 298.15) + h_residual0) * (
                    mass_flow_in0 + mass_steam_flow0)
            del_h_reactant1 = (cp_g_feed1 * (t_process_in1 - 298.15) + h_residual1) * (
                    mass_flow_in1 + mass_steam_flow1)
            if del_h_reactant1 == 0 and del_h_reactant0 != 0:
                t_process_in = t_process_in0
            elif del_h_reactant1 != 0 and del_h_reactant0 == 0:
                t_process_in = t_process_in1
            elif del_h_reactant1 != 0 and del_h_reactant0 != 0:
                t = np.linspace(298.15, max(t_process_in1, t_process_in0), 1000).tolist()
                result_heat_capacity = XProperty.heat_capacity(t, {key: value / 100 for key, value in mol_y.items()})
                result_residual_enthalpy = XProperty.residual_enthalpy([p_process], t,
                                                                       {key: value / 100 for key, value in
                                                                        mol_y.items()})
                cp_g_feed = result_heat_capacity[1]
                h_residual = result_residual_enthalpy[0]
                cpdt = cp_g_feed[: - 1] * np.diff(t)
                int_cpdt = abs(np.cumsum(cpdt))
                del_h_product = (mass_flow_in * (int_cpdt + h_residual[:-1]))
                del_h_reactant = del_h_reactant1 + del_h_reactant0
                t_index = [i for i in range(len(del_h_product)) if
                           0.99 * del_h_reactant < del_h_product[i] < 1.01 * del_h_reactant]
                t_process_in = statistics.mean([t[i] for i in t_index]) if t_index else 0

            else:
                t_process_in = 0

        elif not steam_flag and stream_flag:
            t_process_in = t_process_in0
            p_process = p_process0
        elif steam_flag and not stream_flag:
            t_process_in = t_process_in1
            p_process = p_process1
        else:
            return -1
        return {'mass_x': mass_x, 'mol_y': mol_y, 'mass_flow_in': mass_flow_in, 't_process_in': t_process_in,
                'p_process': p_process}

    @staticmethod
    def heat_reaction(mol_y, mass_flow_in, t_process_in, p_process, t_process_out,
                      mw_prc_in, approach_to_equilibrium=0, eos=1, reaction_set='sma', delta_p=120):
        mol_y['H2'] = mol_y['H2'] if 'H2' in mol_y else 0
        mol_y['CH4'] = mol_y['CH4'] if 'CH4' in mol_y else 0
        mol_y['H2O'] = mol_y['H2O'] if 'H2O' in mol_y else 0
        mol_y['CO2'] = mol_y['CO2'] if 'CO2' in mol_y else 0
        mol_y['CO'] = mol_y['CO'] if 'CO' in mol_y else 0
        mol_y['N2'] = mol_y['N2'] if 'N2' in mol_y else 0
        mw_comp_selected = {key: mw_comp[key] for key, value in mol_y.items()}
        tc_selected = {key: t_c[key] for key, value in mol_y.items()}
        pc_selected = {key: p_c[key] for key, value in mol_y.items()}
        omega_selected = {key: omega[key] for key, value in mol_y.items()}
        p_process_out = p_process - delta_p

        molar_flow_in = mass_flow_in / mw_prc_in / 3600
        std_vol_flow_in = molar_flow_in * (273.15 * r_bar) * 3600
        molar_flow_comp = {key: value * molar_flow_in for key, value in mol_y.items()}
        carbon_flow = sum(
            [value * carbon_molc[key] for key, value in molar_flow_comp.items() if key not in {'CO2', 'CO'}])
        steam_carbon_ratio = molar_flow_comp['H2O'] / carbon_flow

        if not t_process_out:
            del_h_reaction, mol_y_final, methane_slip_mol, methane_slip_mass, p_process_out, molar_flow_out, mass_x_final, mw_procee_out = []
        else:
            if eos == 2:
                m = {key: 0.48 + 1.574 * value - 0.176 * value ** 2 for key, value in omega_selected.items()}
                OMEGA = 0.08664
                sai = 0.45748

            else:
                m = {key: 0.37464 + 1.54226 * value - 0.26992 * value ** 2 for key, value in omega_selected.items()}
                OMEGA = 0.07779
                sai = 0.45724

            a = {key: sai * r_bar ** 2 * (t_c[key] ** 2) / p_c[key] for key, value in mol_y.items()}
            b = {key: OMEGA * r_bar * t_c[key] / p_c[key] for key, value in mol_y.items()}
            t_red = {key: t_process_out / t_c[key] for key, value in mol_y.items()}
            alpha = {key: (1 + m[key] * (1 - t_red[key] ** 0.5)) ** 2 for key, value in mol_y.items()}
            a_alpha = {key: a[key] * alpha[key] for key, value in mol_y.items()}
            AA = {key: a_alpha[key] * p_process_out / ((r_kpa ** 2) * (t_process_out - approach_to_equilibrium) ** 2)
                  for key, value in mol_y.items()}
            B = {key: b[key] * p_process_out / (r_kpa * (t_process_out - approach_to_equilibrium)) for key, value in
                 mol_y.items()}

            kij = df_kij[mol_y.keys()].pivot_table(columns=['kij'])
            kij = kij[mol_y.keys()].to_dict()

            a_alpha_ij = {}
            A_ij = {}
            for key in mol_y.keys():
                material = kij[key]
                a_alpha_i = a_alpha[key]
                AA_i = AA[key]
                a_alpha_ij[key] = {key: (1 - value) * math.sqrt(a_alpha_i * a_alpha[key]) for key, value in
                                   material.items()}
                A_ij[key] = {key: (1 - value) * math.sqrt(AA_i * AA[key]) for key, value in material.items()}

            eps_heavy = {key: -value / reactant_STOCH[key] for key, value in molar_flow_comp.items() if
                         key not in {'H2', 'CH4', 'H2O', 'CO2', 'CO', 'N2'}}
            # Calculating the eqilibrium constant (K)
            a = -20.55
            b = -2.292e4
            c = 7.195
            d = -2.949e-3
            aa = a + b / (t_process_out - approach_to_equilibrium) + c * math.log(
                t_process_out - approach_to_equilibrium) + d * (t_process_out - approach_to_equilibrium)
            k = [0, 0, 0]
            k[0] = math.exp(aa)
            a = -12.11
            b = 5318.7
            c = 1.012
            d = 1.144e-4
            aa = a + b / (t_process_out - approach_to_equilibrium) + c * math.log(
                t_process_out - approach_to_equilibrium) + d * (t_process_out - approach_to_equilibrium)
            k[1] = math.exp(aa)
            k[2] = k[0] * k[1]

            def smr_reaction_equilibria(eps):
                eps0 = eps[0]
                eps1 = eps[1]
                result_convert_heavy = XProperty.converting_heavy_hydrocarbons(molar_flow_comp, eps0, eps1)
                mol_y_final = {key: value for key, value in result_convert_heavy[0].items() if
                               key in {'H2', 'CH4', 'H2O', 'CO2', 'CO', 'N2', 'Ar'}}
                a_alpha_mix = 0
                A_mix = 0
                sum_mol_alpha = dict()
                for ikey, ivalue in mol_y_final.items():
                    sum_mol_alpha[ikey] = sum(
                        [value * (a_alpha_ij[ikey][key]) for key, value in mol_y_final.items()])

                    a_alpha_mix += (ivalue * sum_mol_alpha[ikey])

                    A_mix += (ivalue * sum([value * (A_ij[key][ikey]) for key, value in mol_y_final.items()]))
                B_mix = sum([mol_y_final[key] * B[key] for key, value in mol_y_final.items()])

                z = eps[2]
                f = [0, 0, 0]
                f[2] = ((z ** 3) - (1 - B_mix) * (z ** 2) + (A_mix - 3 * (B_mix ** 2) - 2 * B_mix) * z - (
                        A_mix * B_mix - (B_mix ** 2) - B_mix ** 3))
                if eos == 1:

                    ln_phi_i = {key: (B[key] * (z - 1) / B_mix) - math.log(z - B_mix) + A_mix / (2.828 * B_mix) * (
                            B[key] / B_mix - 2 * sum_mol_alpha[key]) * math.log(
                        (z + 2.414 * B_mix) / (z - 0.414 * B_mix)) for key, value in
                                mol_y_final.items() if
                                key in {'H2', 'CH4', 'H2O', 'CO2', 'CO'}}
                elif eos == 2:
                    ln_phi_i = {key: (B[key] * (z - 1) / B_mix) - math.log(z - B_mix) +
                                     A_mix / (1 * B_mix) * (B[key] / B_mix - 2 * sum_mol_alpha[key])
                                     * math.log(1 + B_mix / z) for key, value in
                                mol_y_final.items() if
                                key in {'H2', 'CH4', 'H2O', 'CO2', 'CO'}}
                phi_i = {key: math.exp(ln_phi_i[key]) for key, value in ln_phi_i.items()}

                f[0] = phi_i['CO'] * mol_y_final['CO'] * ((phi_i['H2'] * mol_y_final['H2']) ** 3) / (
                        phi_i['CH4'] * mol_y_final['CH4'] * phi_i['H2O'] * mol_y_final['H2O']) - k[0] * (
                               p_process_out / 100) ** (-2)  # %SMR
                f[1] = phi_i['CO2'] * mol_y_final['CO2'] * ((phi_i['H2'] * mol_y_final['H2']) ** 4) / (
                        phi_i['CH4'] * mol_y_final['CH4'] * (phi_i['H2O'] * mol_y_final['H2O']) ** 2) - k[2] * (
                               p_process_out / 100) ** (-2)

                return f

            eps0 = [0.1, 0.1, 1]
            eps = fsolve(smr_reaction_equilibria, np.array(eps0))

            eps0 = eps[0]
            eps1 = eps[1]

            result_convt_heavy = XProperty.converting_heavy_hydrocarbons(molar_flow_comp, eps0, eps1)
            mol_y_final = {key: value for key, value in result_convt_heavy[0].items()}
            # if key in {'H2', 'CH4', 'H2O', 'CO2', 'CO', 'N2','Ar'}  }
            del_H_NON_CH4 = result_convt_heavy[1]
            mol_y_copy = mol_y_final.copy()

            mol_y_copy['basis'] = 'mole'
            mass_x_final = XProperty.convert_mol_mass(mol_y_copy)['mass']
            methane_slip_mol = mol_y_final['CH4'] / sum(
                [mol_y_final[key] for key, value in mol_y_final.items() if key not in {'H2O'}]) * 100
            methane_slip_mass = mass_x_final['CH4'] / sum(
                [mass_x_final[key] for key, value in mass_x_final.items() if key not in {'H2O'}]) * 100
            mw_prc_out = sum([mol_y_final[key] * mw_comp[key] for key, value in mol_y_final.items()])
            molar_flow_out = molar_flow_in + 2 * eps[0] + 2 * eps[1]

            result_heat_capacity = XProperty.heat_capacity([t_process_in], mol_y)
            result_residual_enthalpy = XProperty.residual_enthalpy([p_process], [t_process_in], mol_y)
            cp_g_feed = result_heat_capacity[3][0]
            h_residual = result_residual_enthalpy[0][0]
            del_H_REACTANT_298 = (cp_g_feed * (t_process_in - 298.15) + h_residual) * (
                    mass_flow_in / 3600)
            result_residual_enthalpy = XProperty.residual_enthalpy([p_process_out], [t_process_out], mol_y_final)

            result_heat_capacity = XProperty.heat_capacity(temperature=[float(t_process_out)], mol_y=mol_y_final)

            cp_g_feed = result_heat_capacity[3]
            h_residual = result_residual_enthalpy[0]
            del_H_PRODUCT_298 = (cp_g_feed * (t_process_out - 298.15) + h_residual) * (
                    mass_flow_in / 3600)

            x = molar_flow_comp['CH4'] - molar_flow_out * mol_y_final['CH4'] - molar_flow_out * mol_y_final['CO2'] + \
                molar_flow_comp['CO2']

            m = (mol_y['CH4'] - mol_y_final['CH4']) / (
                    1 + 2 * mol_y_final['CH4'])
            n = mol_y_final['CO2'] * (1 + 2 * m) - mol_y['CO2']  # % mole fraction of STEAM consumed by reaction 2
            del_H_01 = (m * del_H_REF_298['H2'] + n * (del_H_REF_298['CH4'])) * molar_flow_in  # %kW
            del_H_0 = (x * del_H_REF_298['H2'] + (molar_flow_comp['CH4'] - molar_flow_out * mol_y_final['CH4'] - x) * (
                del_H_REF_298['H2O']))
            del_H_reaction = -del_H_REACTANT_298 + del_H_0 + del_H_PRODUCT_298 + del_H_NON_CH4
            return del_H_reaction, mol_y_final, steam_carbon_ratio, methane_slip_mol, methane_slip_mass, p_process_out, molar_flow_out, molar_flow_in, mass_x_final, std_vol_flow_in, mw_prc_out

    @staticmethod
    def fuel_property(fuel_component_no=0, mol_y_fuel=0, t_fuel_in_prc1=0, t_fuel_in_prc2=0, lhv_mole_in=0,
                      lhv_mass_in=0,
                      mw_mix_fuel_prc1=0, t_air_in=0, excess_air=0,
                      p_air_in=0, humidity_air_in=0, t_humidity_air=0, gas_fuel_component=[], fuel_procedure='gasfuel',
                      calculation_procedure=0,
                      c_content=0,
                      h_content=0, o_content=0, n_content=0, s_content=0,
                      fuel_flow_in_prc1=0, fuel_flow_in_prc2=0, combustion_rate=0, physical_property_fuel=0):
        # r_bar = 8.3144598e-2
        # r_kpa = 8.314
        # mw_comp_selected = {key: mw_comp[key] for key, value in mol_y_fuel.items()}
        # tc_selected = {key: t_c[key] for key, value in mol_y_fuel.items()}
        # pc_selected = {key: p_c[key] for key, value in mol_y_fuel.items()}
        # omega_selected = {key: omega[key] for key, value in mol_y_fuel.items()}
        # lhv_component = physical_property_fuel[:, 18]
        # o2_req_component = physical_property_fuel[:, 25]
        # carbon_fuel = physical_property_fuel[:, 20]
        # h2_fuel = physical_property_fuel[:, 21]
        # ar_fuel = physical_property_fuel[:, 24]
        # n2_fuel = physical_property_fuel[:, 22]
        # s_fuel = physical_property_fuel[:, 23]

        if 'fixed_combustion' not in calculation_procedure:
            if fuel_procedure == 'liquidfuel':
                pass
                # check if the number of items  in LHV_MOLE_IN, FUEL_FLOW_IN_prc1, T_FUEL_IN_prc1 for liquid fuels are the same (warning). then select the fuels with complete information and ignore incomplete informatio
            elif fuel_procedure == 'gasfuel':
                # fire_box['gasFuel1ID']
                mol_y_fuel_sum = True if sum(mol_y_fuel.values()) == 100 else False
                # check if the number of items  in MOL_Y_fuel, T_FUEL_IN_prc2 for gas fuels are the same (warning). then select the fuels with complete information and ignore incomplete information
                # if the fuel information (MOL_Y_fuel, FUEL_FLOW_IN_prc2, T_FUEL_IN_prc2) of all fuels are complete, then empty the last FUEL_FLOW_IN_prc2

        elif 'fixed_combustion' in calculation_procedure:
            if fuel_procedure == 'liquidfuel':
                pass
                # check if the number of items  in LHV_MOLE_IN, FUEL_FLOW_IN_prc, T_FUEL_IN_prc for liquid fuels are the same (warning). then select the fuels with complete information and ignore incomplete informatio
            elif fuel_procedure == 'gasfuel':
                gas_fuel = dict()
                for item in gas_fuel_component:
                    _sum = sum(
                        [float(value) for key, value in item.items() if key not in {'basis', 'id', 'name', 'uuid'}])
                    if 99.9 <= _sum <= 100.1:
                        if calculation_procedure['gas_fuel_id_4'] == item[
                            'id'] and 'gas_fuel_temp_4' in calculation_procedure:
                            gas_fuel['gas_fuel_four'] = 1 if 'gas_fuel_flow_4' in calculation_procedure else 0

                        if calculation_procedure['gas_fuel_id_3'] == item[
                            'id'] and 'gas_fuel_temp_3' in calculation_procedure:
                            gas_fuel['gas_fuel_three'] = 1 if 'gas_fuel_flow_3' in calculation_procedure else 0

                        if calculation_procedure['gas_fuel_id_2'] == item[
                            'id'] and 'gas_fuel_temp_2' in calculation_procedure:
                            gas_fuel['gas_fuel_two'] = 1 if 'gas_fuel_flow_2' in calculation_procedure else 0

                        if calculation_procedure['gas_fuel_id_1'] == item[
                            'id'] and 'gas_fuel_temp_1' in calculation_procedure:
                            gas_fuel['gas_fuel_one'] = 1 if 'gas_fuel_flow_1' in calculation_procedure else 0

                if int(calculation_procedure['fixed_combustion']) > 0:

                    freedom_degree = len(gas_fuel) - sum(gas_fuel.values())
                    print(freedom_degree)
                    if freedom_degree == 0:
                        gas_fuel[list(gas_fuel.keys())[0]] = 0
                    elif freedom_degree > 1:
                        for key, value in gas_fuel.copy().items():
                            freedom_degree = len(gas_fuel) - sum(gas_fuel.values())
                            if value == 0 and freedom_degree > 1:
                                del gas_fuel[key]
                else:
                    gas_fuel = dict(filter(lambda item: item[1] == 1, gas_fuel.items()))

                print(gas_fuel)
                # print(len(gas_fuel) - sum(gas_fuel.values()))
                # check if the data in gasFuelID && sum(gasFuelCompositions['id'])=100 && gasFuelTemp['gasFuelID'] for each gas fuels is filled (otherwise delete ID).
                if 'fixedCombustion':
                    pass
                    # if the fuel information (MOL_Y_fuel, FUEL_FLOW_IN_prc2, T_FUEL_IN_prc2) of all fuels are complete, then empty the last FUEL_FLOW_IN_prc2
                else:
                    pass
                    # check if the number of items  in MOL_Y_fuel, FUEL_FLOW_IN_prc, T_FUEL_IN_prc for gas fuels are the same (warning). then select the fuels with complete information and ignore incomplete information

        return 'done'
