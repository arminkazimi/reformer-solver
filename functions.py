from init import *


class Solver:
    r_bar = 8.3144598e-2

    def __init__(self):
        self.load_physical_properties()
        self.load_smr_wgs_ratedata()
        self.load_smr_wgs_adsoptiondata()

    def load_physical_properties(self):
        df = pd.read_csv('process_composition.csv', index_col=['process_composition'])
        self.t_c = df.to_dict()['Tc']
        self.t_b = df.to_dict()['Tb']
        self.p_c = df.to_dict()['Pc']
        self.v_c = {key: (value * 1e-3) for key, value in df.to_dict()['Vc'].items()}
        self.z_c = {key: (value * self.v_c[key] / (self.r_bar * self.t_c[key])) for key, value in self.p_c.items()}
        self.omega = df.to_dict()['omega']
        self.mw_comp = df.to_dict()['mw']
        self.a0_cp = df.to_dict()['a0cp']
        self.a1_cp = df.to_dict()['a1cp']
        self.a2_cp = df.to_dict()['a2cp']
        self.a3_cp = df.to_dict()['a3cp']
        self.a4_cp = df.to_dict()['a4cp']
        self.pow0_cp = df.to_dict()['pow0_cp']
        self.pow1_cp = df.to_dict()['pow1_cp']
        self.pow2_cp = df.to_dict()['pow2_cp']
        self.pow3_cp = df.to_dict()['pow3_cp']
        self.pow4_cp = df.to_dict()['pow4_cp']
        self.dipole = df.to_dict()['dipole']
        self.a_landa = df.to_dict()['A_landa']
        self.b_landa = df.to_dict()['B_landa']
        self.c_landa = df.to_dict()['C_landa']
        self.d_landa = df.to_dict()['D_landa']

    def residual_enthalpy(self, p_process, t_process, mol_y):
        t_pc = sum([value * self.t_c[key] for key, value in mol_y.items()])
        v_pc = sum([value * self.v_c[key] for key, value in mol_y.items()])
        z_pc = sum([value * self.z_c[key] for key, value in mol_y.items()])
        omega_p = sum([value * self.omega[key] for key, value in mol_y.items()])
        p_pc = 100 * z_pc * self.r_bar * t_pc / v_pc
        p_pr = p_process / p_pc
        t_pr = t_process / t_pc
        mw_prc = sum([value * self.mw_comp[key] for key, value in mol_y.items()])
        b0 = 0.083 - 0.422 / (t_pr ** 1.6)
        return "residual_enthalpy"

    def heat_capacity(self, temperature, mol_y):

        mw_comp = {key: self.mw_comp[key] for key, value in mol_y.items()}
        a0_cp = {key: self.a0_cp[key] for key, value in mol_y.items()}
        pow0_cp = {key: self.pow0_cp[key] for key, value in mol_y.items()}
        a1_cp = {key: self.a1_cp[key] for key, value in mol_y.items()}
        pow1_cp = {key: self.pow1_cp[key] for key, value in mol_y.items()}
        a2_cp = {key: self.a2_cp[key] for key, value in mol_y.items()}
        pow2_cp = {key: self.pow2_cp[key] for key, value in mol_y.items()}
        a3_cp = {key: self.a3_cp[key] for key, value in mol_y.items()}
        pow3_cp = {key: self.pow3_cp[key] for key, value in mol_y.items()}
        a4_cp = {key: self.a4_cp[key] for key, value in mol_y.items()}
        pow4_cp = {key: self.pow4_cp[key] for key, value in mol_y.items()}

        mw_comp = np.array(list(mw_comp.values())).reshape(len(mol_y), 1)

        a0_cp = np.array(list(a0_cp.values())).reshape(len(mol_y), 1)
        pow0_cp = np.array(list(pow0_cp.values())).reshape(len(mol_y), 1)
        a1_cp = np.array(list(a1_cp.values())).reshape(len(mol_y), 1)
        pow1_cp = np.array(list(pow1_cp.values())).reshape(len(mol_y), 1)
        a2_cp = np.array(list(a2_cp.values())).reshape(len(mol_y), 1)
        pow2_cp = np.array(list(pow2_cp.values())).reshape(len(mol_y), 1)
        a3_cp = np.array(list(a3_cp.values())).reshape(len(mol_y), 1)
        pow3_cp = np.array(list(pow3_cp.values())).reshape(len(mol_y), 1)
        a4_cp = np.array(list(a4_cp.values())).reshape(len(mol_y), 1)
        pow4_cp = np.array(list(pow4_cp.values())).reshape(len(mol_y), 1)
        mol_y_arr = np.array(list(mol_y.values())).reshape(len(mol_y), 1)
        temperature = np.array(temperature * len(mol_y)).reshape(len(mol_y), len(temperature))

        cp_comp = mw_comp * (
                a0_cp * temperature ** pow0_cp + a1_cp * temperature ** pow1_cp + a2_cp * temperature ** pow2_cp + a3_cp * temperature ** pow3_cp + a4_cp * temperature ** pow4_cp)

        cp_g_mol = sum(mol_y_arr * cp_comp)

        cp_g_mass = cp_g_mol / sum(mol_y_arr * mw_comp)
        print(cp_g_mass.shape)
        return "heat_capacity"

    def convert_mol_mass(self, data, data_csv, response):
        result_list = list()
        for obj in data:
            if obj['basis'] == 'mass':
                MASS_X_res = {key: value for key, value in obj.items() if
                              key != 'basis' and key != 'id' and key != 'name' and key != 'uuid'}
                MOL_Y_res = {key: float(100 * value / data_csv[key]) for key, value in MASS_X_res.items()}
                res = {key: float(value / data_csv[key]) for key, value in MASS_X_res.items()}
                MW = sum(res.values()) + 1e-8
                # print('MW object ', obj['id'], 'is: ', MW)
                MOL_Y_res = {key: float(value / MW) for key, value in MOL_Y_res.items()}
                # print('MOL Y res object ', obj['id'], 'is: ', MOL_Y_res)
                result = {'id': obj['id'], 'name': obj['name'] if obj['name'] else None,
                          'data': {'mass': MASS_X_res, 'mol': MOL_Y_res}}
                # result = {'id': obj['id'], 'data': {'mass': MASS_X_res, 'mol': MOL_Y_res}}
                result_list.append(result)
            elif obj['basis'] == 'mole':
                MOL_Y_res = {key: value for key, value in obj.items() if
                             key != 'basis' and key != 'id' and key != 'name' and key != 'uuid'}
                res = {key: value * data_csv[key] for key, value in MOL_Y_res.items()}
                MW = sum(res.values()) + 1e-8
                # print('MW object ', obj['id'], 'is: ', MW)
                MASS_X_res = {key: float(100 * data_csv[key] * value / MW) for key, value in MOL_Y_res.items()}
                # print('mass x res object ', obj['id'], 'is: ', MASS_X_res)
                result = {'id': obj['id'], 'name': obj['name'] if 'name' in obj.keys() else None,
                          'data': {'mass': MASS_X_res, 'mol': MOL_Y_res}}
                # result = {'id': obj['id'], 'data': {'mass': MASS_X_res, 'mol': MOL_Y_res}}
                result_list.append(result)
        response['physicalProperties']['processCompositions'].clear()
        response['physicalProperties']['processCompositions'] = result_list
        return response

    def rate_parameters(self):

        return 'Rate_parameters'
        # return  [a_ad,del_h_ad,]

    def load_smr_wgs_ratedata(self):
        df = pd.read_csv('smr_wgs_ratedata.csv')
        df = df.pivot_table(columns='SMR_WGS_RATEDATA')
        self.e = df.to_dict()['E']
        self.a = df.to_dict()['A']
        self.del_H_REF_298 = df.to_dict()['del_H_REF_298']
        self.del_H_REF_948 = df.to_dict()['del_H_REF_948']
        self.diff_noo = df.to_dict()['diff_noo']
        self.CO_STOCH = df.to_dict()['CO_STOCH']
        self.H2_STOCH = df.to_dict()['H2_STOCH']
        self.reactant_STOCH = df.to_dict()['REACTANT_STOCH']
        self.H2O_STOCH = df.to_dict()['H2O_STOCH']
        self.CO2_STOCH = df.to_dict()['CO2_STOCH']
        self.N2_STOCH = df.to_dict()['N2_STOCH']

    def load_smr_wgs_adsoptiondata(self):
        df = pd.read_csv('smr_wgs_adsoptiondata.csv')
        df = df.pivot_table(columns='SMR_WGS_ADSOPTIONDATA')
        self.a_ad = df.to_dict()['A_ad']
        self.del_H_ad = df.to_dict()['del_H_ad']

    def gas_viscosity_conductivity(self, temperature, df, mol_y):
        f_q = dict()
        for t in range(temperature):
            tr = t / self.t_c

            component_number = len(df)
            vis_r = 52. * self.dipole * self.p_c / (self.t_c ** 2)
            kesi = 0.176 * (self.t_c / (self.mw_comp ** 3) * (self.p_c ** 4)) ** 1.6
            f_p = np.sign(np.sign(0.022 - vis_r) + 1) * 1 + np.sign(
                np.sign(0.075 - vis_r) * np.sign(vis_r - 0.0220001) + 1) * (1 + 30.55 * (292 - self.z_c) ** 1.72) * abs(
                0.96 + 1 * (tr - 0.7))
            if 'H2' in f_q.keys():
                pass
            viscosity_comp = f_p * f_q / kesi * (0.807 * tr ** 0.618 - 0.357 * math.exp(-0.449 * tr) + 0.34 * math.exp(
                -4.058 * tr) + 0.018) * 1e-7
            phi_ij = ((1 + ((viscosity_comp / (viscosity_comp)) ** 0.5) * (
                    self.mw_comp / self.mw_comp) ** 0.25) ** 2) / (8 * (1 + (self.mw_comp / self.mw_comp)) ** 0.5)
            landa_comp = self.a_landa + self.b_landa * t + self.c_landa * t ** 2 + self.d_landa * t ** 3
            thermal_conductivity_g = 0
            viscosity_g = 0
            for mol in mol_y:
                thermal_conductivity_g = thermal_conductivity_g + (mol * landa_comp / sum(mol_y * phi_ij))
                viscosity_g = viscosity_g + (mol * viscosity_comp / sum(mol_y * phi_ij))

    def converting_heavy_hydrocarbons(self, molar_flow_comp):
        molar_flow_comp['H2'] += 0
        molar_flow_comp['CH4'] += 0
        molar_flow_comp['H2O'] += 0
        molar_flow_comp['CO2'] += 0
        molar_flow_comp['CO'] += 0
        molar_flow_comp['N2'] += 0

        del_H_NON_CH4 = sum([value * self.del_H_REF_298[key] for key, value in molar_flow_comp.items() if
                             key not in {'H2', 'CH4', 'H2O', 'CO2', 'CO', 'N2'}])
        eps01 = {key: -value / self.reactant_STOCH[key] for key, value in molar_flow_comp.items() if
                 key not in {'H2', 'CH4', 'H2O', 'CO2', 'CO', 'N2'}}
        molar_flow_in = sum(molar_flow_comp.values())
        # mol_y_in = dict()
        denominator = molar_flow_in + sum([value * self.diff_noo[key] for key, value in eps01.items()])
        mol_y_in = {key: (molar_flow_comp[key] + (value * self.reactant_STOCH[key]) / denominator) for key, value in
                    eps01.items()}

        mol_y_in['H2'] = (molar_flow_comp['H2'] + sum(
            [value * self.H2_STOCH[key] for key, value in eps01.items()])) / denominator
        mol_y_in['CH4'] = molar_flow_comp['CH4'] / denominator
        mol_y_in['H2O'] = (molar_flow_comp['H2O'] - sum(
            [value * self.H2_STOCH[key] for key, value in eps01.items()])) / denominator
        mol_y_in['CO2'] = (molar_flow_comp['CO2'] * eps01['CH4'] + sum(
            [value * self.CO2_STOCH[key] for key, value in eps01.items()])) / (
                                  molar_flow_in + 2 * eps01['H2'] + 2 * eps01['CH4'] + sum(
                              [value * self.diff_noo[key] for key, value in eps01.items()]))
        mol_y_in['CO'] = (molar_flow_comp['CO'] + sum(
            [value * self.CO_STOCH[key] for key, value in eps01.items()])) / denominator
        mol_y_in['N2'] = (molar_flow_comp['N2'] + sum(
            [value * self.N2_STOCH[key] for key, value in eps01.items()])) / (
                                 molar_flow_in + 2 * eps01['H2'] + 2 * eps01['CH4'] + sum(
                             [value * self.diff_noo[key] for key, value in eps01.items()]))
        return mol_y_in, del_H_NON_CH4
