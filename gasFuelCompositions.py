from init import *


class GasFuelCompositions:
    def __init__(self):
        df = pd.read_csv('gas_fuel_composition.csv', index_col=['process_composition'])
        self.t_c = df.to_dict()['Tc']
        # self.t_b = df.to_dict()['Tb']
        # self.p_c = df.to_dict()['Pc']
        # self.v_c = {key: (value * 1e-3) for key, value in df.to_dict()['Vc'].items()}
        # self.z_c = {key: (value * self.v_c[key] / (self.r_bar * self.t_c[key])) for key, value in self.p_c.items()}
        # self.omega = df.to_dict()['omega']
        # self.mw_comp = df.to_dict()['mw']
        # self.a0_cp = df.to_dict()['a0cp']
        # self.a1_cp = df.to_dict()['a1cp']
        # self.a2_cp = df.to_dict()['a2cp']
        # self.a3_cp = df.to_dict()['a3cp']
        # self.a4_cp = df.to_dict()['a4cp']
        # self.pow0_cp = df.to_dict()['pow0_cp']
        # self.pow1_cp = df.to_dict()['pow1_cp']
        # self.pow2_cp = df.to_dict()['pow2_cp']
        # self.pow3_cp = df.to_dict()['pow3_cp']
        # self.pow4_cp = df.to_dict()['pow4_cp']

